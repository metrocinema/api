-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema metrocinemas
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema metrocinemas
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `metrocinemas` ;
USE `metrocinemas` ;

-- -----------------------------------------------------
-- Table `metrocinemas`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`user` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`user` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(20) NULL,
  `last_name` VARCHAR(20) NULL,
  `user_type` VARCHAR(5) NULL,
  `active` VARCHAR(5) NULL,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`actor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`actor` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`actor` (
  `id_actor` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(20) NULL,
  `last_name` VARCHAR(20) NULL,
  PRIMARY KEY (`id_actor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`director`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`director` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`director` (
  `id_director` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(20) NULL,
  `last_name` VARCHAR(20) NULL,
  PRIMARY KEY (`id_director`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`movie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`movie` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`movie` (
  `id_movie` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NULL,
  `country` VARCHAR(20) NULL,
  `year` VARCHAR(10) NULL,
  `clasification` VARCHAR(10) NULL,
  `duration` VARCHAR(10) NULL,
  `gender` VARCHAR(10) NULL,
  `sinopsys` VARCHAR(200) NULL,
  `id_actor` INT NOT NULL,
  `id_director` INT NOT NULL,
  `url_trailer` VARCHAR(150) NULL,
  `url_cover` VARCHAR(150) NULL,
  `active` VARCHAR(5) NULL,
  PRIMARY KEY (`id_movie`),
  INDEX `fk_Movies_Actors_idx` (`id_actor` ASC),
  INDEX `fk_Movies_Directors1_idx` (`id_director` ASC),
  CONSTRAINT `fk_Movies_Actors`
    FOREIGN KEY (`id_actor`)
    REFERENCES `metrocinemas`.`actor` (`id_actor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Movies_Directors1`
    FOREIGN KEY (`id_director`)
    REFERENCES `metrocinemas`.`director` (`id_director`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`room`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`room` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`room` (
  `id_room` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NULL,
  `capacity` VARCHAR(45) NULL,
  `active` VARCHAR(5) NULL,
  PRIMARY KEY (`id_room`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`movie_function`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`movie_function` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`movie_function` (
  `id_function` INT NOT NULL AUTO_INCREMENT,
  `id_movie` INT NOT NULL,
  `id_room` INT NOT NULL,
  `movie_schedule` VARCHAR(45) NULL,
  `start_date` VARCHAR(45) NULL,
  `finish_date` VARCHAR(45) NULL,
  `price` VARCHAR(10) NULL,
  `active` VARCHAR(5) NULL,
  PRIMARY KEY (`id_function`),
  INDEX `fk_Functions_Movies1_idx` (`id_movie` ASC),
  INDEX `fk_Functions_Rooms1_idx` (`id_room` ASC),
  CONSTRAINT `fk_Functions_Movies1`
    FOREIGN KEY (`id_movie`)
    REFERENCES `metrocinemas`.`movie` (`id_movie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Functions_Rooms1`
    FOREIGN KEY (`id_room`)
    REFERENCES `metrocinemas`.`room` (`id_room`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`ticket`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`ticket` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`ticket` (
  `id_ticket` INT NOT NULL AUTO_INCREMENT,
  `id_movie` INT NOT NULL,
  `id_room` INT NOT NULL,
  `id_function` INT NOT NULL,
  `seats` VARCHAR(45) NULL,
  PRIMARY KEY (`id_ticket`),
  INDEX `fk_Tickets_Movies1_idx` (`id_movie` ASC),
  INDEX `fk_Tickets_Rooms1_idx` (`id_room` ASC),
  INDEX `fk_Tickets_Functions1_idx` (`id_function` ASC),
  CONSTRAINT `fk_Tickets_Movies1`
    FOREIGN KEY (`id_movie`)
    REFERENCES `metrocinemas`.`movie` (`id_movie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tickets_Rooms1`
    FOREIGN KEY (`id_room`)
    REFERENCES `metrocinemas`.`room` (`id_room`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tickets_Functions1`
    FOREIGN KEY (`id_function`)
    REFERENCES `metrocinemas`.`movie_function` (`id_function`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`seat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`seat` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`seat` (
  `id_seat` INT NOT NULL AUTO_INCREMENT,
  `seats` VARCHAR(45) NULL,
  `id_room` INT NOT NULL,
  `id_function` INT NOT NULL,
  PRIMARY KEY (`id_seat`),
  INDEX `fk_Seats_Rooms1_idx` (`id_room` ASC),
  INDEX `fk_Seats_Functions1_idx` (`id_function` ASC),
  CONSTRAINT `fk_Seats_Rooms1`
    FOREIGN KEY (`id_room`)
    REFERENCES `metrocinemas`.`room` (`id_room`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Seats_Functions1`
    FOREIGN KEY (`id_function`)
    REFERENCES `metrocinemas`.`movie_function` (`id_function`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`ticket_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`ticket_detail` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`ticket_detail` (
  `id_ticket_detail` INT NOT NULL AUTO_INCREMENT,
  `card_number` VARCHAR(45) NULL,
  `id_ticket` INT NOT NULL,
  `id_user` INT NOT NULL,
  `email` VARCHAR(45) NULL,
  `id_seat` INT NOT NULL,
  PRIMARY KEY (`id_ticket_detail`),
  INDEX `fk_TIcketsDetail_Tickets1_idx` (`id_ticket` ASC),
  INDEX `fk_TIcketsDetail_Users1_idx` (`id_user` ASC),
  INDEX `fk_TIcketsDetail_Seats1_idx` (`id_seat` ASC),
  CONSTRAINT `fk_TIcketsDetail_Tickets1`
    FOREIGN KEY (`id_ticket`)
    REFERENCES `metrocinemas`.`ticket` (`id_ticket`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TIcketsDetail_Users1`
    FOREIGN KEY (`id_user`)
    REFERENCES `metrocinemas`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TIcketsDetail_Seats1`
    FOREIGN KEY (`id_seat`)
    REFERENCES `metrocinemas`.`seat` (`id_seat`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`email`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `metrocinemas`.`email` ;

CREATE TABLE IF NOT EXISTS `metrocinemas`.`email` (
  `id_email` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `email` VARCHAR(45) NULL,
  `password` VARCHAR(20) NULL,
  INDEX `fk_email_user1_idx` (`id_user` ASC),
  PRIMARY KEY (`id_email`),
  CONSTRAINT `fk_email_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `metrocinemas`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
