# CASE 1: INSERT a new function MUST pre-exist MOVIE AND ROOM
# get the foreign key with type of the room and movie name

INSERT INTO Functions 
	SET
		Movies_idMovies = (
			SELECT idMovies
            FROM Movies
            WHERE Name = 'avengers'
        ),
        Rooms_idRooms = (
			SELECT idRooms
            FROM Rooms
            WHERE Type = 'IMAX'
        ),
        Schedule = 'monday and wednesday 5-7 pm',
        StartDate = 'sep 30',
        FinishDate = 'oct 06',
        Price = '80',
        Active = true
        