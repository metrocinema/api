UPDATE TicketsDetail
	SET
		CardNumber = '12345678901234',
        Tickets_idTickets = (
			SELECT idTickets
            FROM Tickets
            WHERE last_insert_id()
        ),
        Users_idUsers = (
			SELECT idUsers
            FROM Users
            WHERE FirstName = 'David'
        ),
        Email = 'david_5043@hotmail.com',
        Seats_idSeats = (
			SELECT idSeats
            FROM Seats
            WHERE Seats = '1B'
        )
	WHERE idTicketsDetail = 1;