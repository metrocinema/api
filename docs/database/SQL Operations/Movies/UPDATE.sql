UPDATE Movies
	SET
		Name = 'avengers',
		Country = 'Mexico',
		Year = '2017',
		Clasification = 'A',
		Duration = '120',
		Gender = 'Romantic',
		Sinopsys = 'I love Taylor swift',
		URLTrailer = 'https://www.youtube.com/watch?v=VuNIsY6JdUw',
		URLCover = 'https://www.elespectador.com/sites/default/files/johnason_0.jpg'
	WHERE idMovies = 11;