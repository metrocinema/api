const { Rooms } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class RoomsController {
  /**
   * Retrieve all the rooms in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllRooms(req, res, next) {
    Rooms.find()
      .then(rooms => {
        if (rooms.length === 0) {
          return next(send404('There are no rooms in the database'));
        }
        return res.send(rooms);
      })
      .catch(next);
  }

  /**
   * Retrieve the ID from room in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneRoom(req, res, next) {
    const {
      params: { id }
    } = req;

    Rooms.find(id)
      .then(room => {
        if (!room.id_room) {
          return next(send404('The room does not exists in the database'));
        }

        return res.send(room);
      })
      .catch(next);
  }

  /**
   * Create a new room and retrieve them back a response code
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postRoom(req, res, next) {
    const {
      body: { type, capacity, active }
    } = req;
    Rooms.insert({
      type,
      capacity,
      active: active || 1
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create room'));
        }

        return res.status(201).send({
          id_room: insertId,
          type,
          capacity,
          active
        });
      })
      .catch(next);
  }

  /**
   * Modify the complete room data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putRoom(req, res, next) {
    const {
      body: { id_room, type, capacity, active },
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Rooms.updateById(id, {
      id_room: id,
      type,
      capacity,
      active
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('There room data could not be updated'));
        }

        return res.send({
          id_room,
          type,
          capacity,
          active
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one room using it's id, with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchRoom(req, res, next) {
    const { body } = req;
    const { id } = req.params;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Rooms.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The room data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one movie according to his id, and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteRoom(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Rooms.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The room data could not be updated'));
        }

        return res.send({ message: 'ok' });
      })
      .catch(next);
  }
};
