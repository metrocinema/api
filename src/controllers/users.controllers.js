const { Users } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class UsersController {
  /**
   * Retrieve all the users in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllUsers(req, res, next) {
    Users.find()
      .then(users => {
        if (users.length === 0) {
          return next(send404('There are no users in the database'));
        }
        return res.send(users);
      })
      .catch(next);
  }

  /**
   * Creates a user and retrieve them back in response to the creation with exit
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postUSer(req, res, next) {
    const {
      body: { first_name, last_name, user_type, active }
    } = req;
    Users.insert({
      first_name,
      last_name,
      user_type,
      active: active || 1
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create user'));
        }

        return res.status(201).send({
          id_user: insertId,
          first_name,
          last_name,
          user_type,
          active
        });
      })
      .catch(next);
  }

  /**
   * Ends the sessions of one user
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static logoutUser(req, res, next) {
    return res.send('loged out with exit');
  }

  /**
   * Grants access to the user, allowing him to do more actions
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static loginUser(req, res, next) {
    return res.send('user loged in');
  }

  /**
   * Retrieves only a user by it's id
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getUser(req, res, next) {
    const {
      params: { id }
    } = req;

    Users.find(id)
      .then(user => {
        if (!user.id_user) {
          return next(send404('The user does not exists in the database'));
        }

        return res.send(user);
      })
      .catch(next);
  }

  /**
   * Modify the complete user's data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putUser(req, res, next) {
    const {
      body: { first_name, last_name, user_type, active }
    } = req;
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Users.updateById(id, {
      id_user: id,
      first_name,
      last_name,
      user_type,
      active
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The user data could not be updated'));
        }

        return res.send({
          id_user: id,
          first_name,
          last_name,
          user_type,
          active
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one user using it's id,
   * with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchUser(req, res, next) {
    const { body } = req;
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Users.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The user data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one user according to his id,
   * and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteUser(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Users.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('There user data could not be deleted'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
