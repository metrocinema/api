const { TicketDetail } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class TicketDetailController {
  /**
   * Retrieve all the tickets detail in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllTicketDetail(req, res, next) {
    TicketDetail.find()
      .then(tickets_detail => {
        if (tickets_detail.length === 0) {
          return next(send404('There are no ticket detail in the database'));
        }
        return res.send(tickets_detail);
      })
      .catch(next);
  }

  /**
   * Retrieve the ID from ticket detail in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneTicketDetail(req, res, next) {
    const {
      params: { id }
    } = req;

    TicketDetail.find(id)
      .then(ticket_detail => {
        if (!ticket_detail.id_ticket_detail) {
          return next(
            send404('The ticket details does not exists in the database')
          );
        }

        return res.send(ticket_detail);
      })
      .catch(next);
  }

  /**
   * Create a new Ticket Detail and retrieve them back a response code
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postTicketDetail(req, res, next) {
    const {
      body: { card_number, id_ticket, id_user, email, id_seat }
    } = req;
    TicketDetail.insert({
      card_number,
      id_ticket,
      id_user,
      email,
      id_seat
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create ticket detail'));
        }

        return res.status(201).send({
          id_ticket_detail: insertId,
          card_number,
          id_ticket,
          id_user,
          email,
          id_seat
        });
      })
      .catch(next);
  }

  /**
   * Modify the complete Ticket Detail data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putTicketDetail(req, res, next) {
    const {
      body: { card_number, id_ticket, id_user, email, id_seat },
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    TicketDetail.updateById(id, {
      id_ticket_detail: id,
      card_number,
      id_ticket,
      id_user,
      email,
      id_seat
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The ticket details data could not be updated'));
        }

        return res.send({
          id_ticket_detail: id,
          card_number,
          id_ticket,
          id_user,
          email,
          id_seat
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one ticket detail using it's id, with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchTicketDetail(req, res, next) {
    const { body } = req;
    const { id } = req.params;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    TicketDetail.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The ticekt details data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one ticket detail according to his id,
   and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteTicketDetail(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    TicketDetail.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows !== 0) {
          return next(send404('The ticket details data could not be updated'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
