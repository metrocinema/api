const UsersController = require('./users.controllers');
const FunctionsController = require('./functions.controllers');
const SeatsController = require('./seats.controllers');
const DirectorsController = require('./directors.controllers');
const ActorsController = require('./actors.controllers');
const MoviesController = require('./movies.controllers');
const RoomsController = require('./rooms.controllers');
const TicketsController = require('./tickets.controllers');
const TicketDetailController = require('./ticket-detail.controllers');
const EmailsController = require('./emails.controllers');

module.exports = {
  UsersController,
  FunctionsController,
  SeatsController,
  DirectorsController,
  ActorsController,
  MoviesController,
  RoomsController,
  TicketsController,
  TicketDetailController,
  EmailsController
};
