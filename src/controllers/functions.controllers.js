const { Functions } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class FunctionsController {
  /**
   * Retrieve all the movie functions in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllFunctions(req, res, next) {
    Functions.find()
      .then(functions => {
        if (functions.length === 0) {
          return next(send404('There are no functions in the database'));
        }
        return res.send(functions);
      })
      .catch(next);
  }

  /**
   * Retrieve the ID from movie function in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneFunction(req, res, next) {
    const {
      params: { id }
    } = req;

    Functions.find(id)
      .then(functions => {
        if (!functions.id_function) {
          return next(send404('The function does not exists in the database'));
        }

        return res.send(functions);
      })
      .catch(next);
  }

  /**
   * Create a new movie function and retrieve them back a response code
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postFunction(req, res, next) {
    const {
      body: {
        id_movie,
        id_room,
        movie_schedule,
        start_date,
        finish_date,
        price,
        active
      }
    } = req;
    Functions.insert({
      id_movie,
      id_room,
      movie_schedule,
      start_date,
      finish_date,
      price,
      active
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create movie function'));
        }

        return res.status(201).send({
          id_function: insertId,
          id_movie,
          id_room,
          movie_schedule,
          start_date,
          finish_date,
          price,
          active
        });
      })
      .catch(next);
  }

  /**
   * Modify the complete movie function data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putFunction(req, res, next) {
    const {
      body: {
        id_movie,
        id_room,
        movie_schedule,
        start_date,
        finish_date,
        price,
        active
      },
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Functions.updateById(id, {
      id_function: id,
      id_movie,
      id_room,
      movie_schedule,
      start_date,
      finish_date,
      price,
      active
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The movie function data could not be updated'));
        }

        return res.send({
          id_function: id,
          id_movie,
          id_room,
          movie_schedule,
          start_date,
          finish_date,
          price,
          active
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one movie function using it's id, with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchFunction(req, res, next) {
    const { body } = req;
    const { id } = req.params;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Functions.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The movie function data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one movie function according to his id, and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteFunction(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Functions.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The movie function data could not be deleted'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
