const { Directors } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class DirectorsController {
  /**
   * Retrieve all the directors in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllDirectors(req, res, next) {
    Directors.find()
      .then(directors => {
        if (directors.length === 0) {
          return next(send404('There are no directors in the database'));
        }
        return res.send(directors);
      })
      .catch(next);
  }

  /**
   * Retrieve a director in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneDirector(req, res, next) {
    const {
      params: { id }
    } = req;

    Directors.find(id)
      .then(director => {
        if (!director.id_director) {
          return next(send404('The director does not exists in the database'));
        }

        return res.send(director);
      })
      .catch(next);
  }

  /**
   * Create a new director and retrieve them back a response code
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postDirector(req, res, next) {
    const {
      body: { first_name, last_name }
    } = req;
    Directors.insert({
      first_name,
      last_name
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create director'));
        }

        return res.status(201).send({
          id_director: insertId,
          first_name,
          last_name
        });
      })
      .catch(next);
  }

  /**
   * Modify the complete director data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putDirector(req, res, next) {
    const {
      body: { first_name, last_name },
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Directors.updateById(id, {
      id_director: id,
      first_name,
      last_name
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('There director data could not be updated'));
        }

        return res.send({
          id_director: id,
          first_name,
          last_name
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one director using it's id,
   * with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchDirector(req, res, next) {
    const { body } = req;
    const { id } = req.params;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Directors.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The director data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one director according to his id,
   * and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteDirector(req, res, next) {
    const {
      body: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Directors.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('There user data could not be updated'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
