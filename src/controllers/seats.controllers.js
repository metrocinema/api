const { Seats } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class SeatsController {
  /**
   * Retrieve all the seats in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllSeats(req, res, next) {
    Seats.find()
      .then(seats => {
        if (seats.length === 0) {
          return next(send404('There are no seats in the database'));
        }
        return res.send(seats);
      })
      .catch(next);
  }

  /**
   * Retrieve a seat by it's id
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneSeat(req, res, next) {
    const {
      params: { id }
    } = req;

    Seats.find(id)
      .then(seat => {
        if (!seat.id_seat) {
          return next(send404('The seat does not exists in the database'));
        }

        return res.send(seat);
      })
      .catch(next);
  }

  /**
   * Creates a seat and retrieve them back in response to the creation with exit
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postSeat(req, res, next) {
    const {
      body: { seats, id_room, id_function }
    } = req;
    Seats.insert({
      seats,
      id_room,
      id_function
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create seat'));
        }

        return res.status(201).send({
          id_seat: insertId,
          seats,
          id_room,
          id_function
        });
      })
      .catch(next);
  }

  /**
   * Modify the complete seat data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putSeat(req, res, next) {
    const {
      body: { seats, id_room, id_function },
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Seats.updateById(id, {
      id_seat: id,
      seats,
      id_room,
      id_function
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The seat data could not be updated'));
        }

        return res.send({
          id_seat: id,
          seats,
          id_room,
          id_function
        });
      })
      .catch(next);
  }

  /**
   * update the complete seat data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchSeat(req, res, next) {
    const {
      body,
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Seats.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The seat data could not be updated'));
        }

        return res.send({
          ...fields,
          id_seat: id
        });
      })
      .catch(next);
  }

  /**
   * delete the complete seat data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteSeat(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Seats.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The seat data could not be deleted'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
