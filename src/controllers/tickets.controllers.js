const { Tickets } = require('../models');
const { send404, send422 } = require('../utils');

/**
 * Retrieve all the tickets in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllTickets = (req, res, next) => {
  Tickets.find()
    .then(tickets => {
      if (tickets.lenght === 0) {
        return next(send404('There are no tickets in the database'));
      }
      return res.send(tickets);
    })
    .catch(next);
};

/**
 * Retrieve the ID from room in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneTicket = (req, res, next) => {
  const {
    params: { id }
  } = req;

  Tickets.find(id)
    .then(ticket => {
      if (!ticket.id_ticket) {
        return next(send404('The ticket does not exist in the database'));
      }

      return res.send(ticket);
    })
    .catch(next);
};

/**
 * Create a new ticket and retrieve them back a response code
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postTicket = (req, res, next) => {
  const {
    body: { id_movie, id_room, id_function, seats }
  } = req;
  Tickets.insert({
    id_movie,
    id_room,
    id_function,
    seats
  })
    .then(({ insertId }) => {
      if (!insertId) {
        return next(send404('could not be posible create ticket'));
      }

      return res.status(201).send({
        id_ticket: insertId,
        id_movie,
        id_room,
        id_function,
        seats
      });
    })
    .catch(next);
};

/**
 * Modify the complete ticket data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putTicket = (req, res, next) => {
  const {
    body: { id_movie, id_room, id_function, seats },
    params: { id }
  } = req;

  if (!id) {
    return next(send422('There are no an id provided to update'));
  }

  Tickets.updateById(id, {
    id_ticket: id,
    id_movie,
    id_room,
    id_function,
    seats
  })
    .then(({ changedRows }) => {
      if (changedRows === 0) {
        return next(send404('The ticket data could not be updated'));
      }

      return res.send({
        id_ticket: id,
        id_movie,
        id_room,
        id_function,
        seats
      });
    })
    .catch(next);
};

/**
 * Update the information of one ticket using it's id, with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchTicket = (req, res, next) => {
  const { body } = req;
  const { id } = req.params;

  if (!id) {
    return next(send422('There are no an id provided to update'));
  }

  const fields = body;

  Tickets.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows === 0) {
        return next(send404('The ticket data could not be updated'));
      }

      return res.send({
        ...fields,
        id_ticket: id
      });
    })
    .catch(next);
};

/**
 * Delete one ticket according to his id, and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteTicket = (req, res, next) => {
  const {
    params: { id }
  } = req;

  if (!id) {
    return next(send422('There are no an id provided to delete'));
  }

  Tickets.deleteById(id)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        return next(send404('The ticket data could not be updated'));
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
