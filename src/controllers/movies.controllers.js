const { Movies } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class MoviesController {
  /**
   * Retrieve all the movies in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllMovies(req, res, next) {
    Movies.find()
      .then(movies => {
        if (movies.length === 0) {
          return next(send404('There are no movies in the database'));
        }
        return res.send(movies);
      })
      .catch(next);
  }

  /**
   * Retrieve the ID from movie in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneMovie(req, res, next) {
    const {
      params: { id }
    } = req;

    Movies.find(id)
      .then(movie => {
        if (!movie.id_movie) {
          return next(send404('The movie does not exists in the database'));
        }

        return res.send(movie);
      })
      .catch(next);
  }

  /**
   * Create a new movie and retrieve them back a response code
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postMovie(req, res, next) {
    const {
      body: {
        name,
        country,
        year,
        clasification,
        duration,
        gender,
        sinopsys,
        id_actor,
        id_director,
        url_trailer,
        url_cover,
        active
      }
    } = req;
    Movies.insert({
      name,
      country,
      year,
      clasification,
      duration,
      gender,
      sinopsys,
      id_actor,
      id_director,
      url_trailer,
      url_cover,
      active
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create movie'));
        }

        return res.status(201).send({
          id_movie: insertId,
          name,
          country,
          year,
          clasification,
          duration,
          gender,
          sinopsys,
          id_actor,
          id_director,
          url_trailer,
          url_cover,
          active
        });
      })
      .catch(next);
  }

  /**
   * Modify the complete movie data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putMovie(req, res, next) {
    const {
      body: {
        name,
        country,
        year,
        clasification,
        duration,
        gender,
        sinopsys,
        id_actor,
        id_director,
        url_trailer,
        url_cover,
        active
      },
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Movies.updateById(id, {
      id_movie: id,
      name,
      country,
      year,
      clasification,
      duration,
      gender,
      sinopsys,
      id_actor,
      id_director,
      url_trailer,
      url_cover,
      active
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The movie data could not be updated'));
        }

        return res.send({
          id_movie: id,
          name,
          country,
          year,
          clasification,
          duration,
          gender,
          sinopsys,
          id_actor,
          id_director,
          url_trailer,
          url_cover,
          active
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one movie using it's id, with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchMovie(req, res, next) {
    const { body } = req;
    const { id } = req.params;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Movies.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows !== 0) {
          return next(send404('The movie data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one movie according to his id, and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteMovie(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Movies.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The movie data could not be deleted'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
