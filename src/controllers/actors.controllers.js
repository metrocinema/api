const { Actors } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class ActorsController {
  /**
   * Retrieve all the actors in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllActors(req, res, next) {
    Actors.find()
      .then(actors => {
        if (actors.length === 0) {
          return next(send404('There are no actors in the database'));
        }
        return res.send(actors);
      })
      .catch(next);
  }

  /**
   * Retrieve a actor in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneActor(req, res, next) {
    const {
      params: { id }
    } = req;

    Actors.find(id)
      .then(actor => {
        if (!actor.id_actor) {
          return next(send404('The actor does not exists in the database'));
        }

        return res.send(actor);
      })
      .catch(next);
  }

  /**
   * Create a new actor and retrieve them back a response code
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postActor(req, res, next) {
    const {
      body: { first_name, last_name }
    } = req;
    Actors.insert({
      first_name,
      last_name
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be posible create actor'));
        }

        return res.status(201).send({
          id_actors: insertId,
          first_name,
          last_name
        });
      })
      .catch(next);
  }

  /**
   * Modify the complete actor data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putActor(req, res, next) {
    const {
      body: { first_name, last_name },
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Actors.updateById(id, {
      id_actor: id,
      first_name,
      last_name
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The actor data could not be updated'));
        }

        return res.send({
          id_actor: id,
          first_name,
          last_name
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one actor using it's id,
   * with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchActor(req, res, next) {
    const { body } = req;
    const { id } = req.params;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Actors.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('There actor data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one actor according to his id,
   * and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteActor(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Actors.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('There actor data could not be updated'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
