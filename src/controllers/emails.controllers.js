const { Emails } = require('../models');
const { send404, send422 } = require('../utils');

module.exports = class EmailsController {
  /**
   * Retrieve all the emails in the database
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getAllEmails(req, res, next) {
    Emails.find()
      .then(emails => {
        if (emails.length === 0) {
          return next(send404('There are no emails in the database'));
        }
        return res.send(emails);
      })
      .catch(next);
  }

  /**
   * Creates a email and retrieve them back in response to the creation with exit
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static postEmail(req, res, next) {
    const {
      body: { id_user, email, password }
    } = req;

    Emails.insert({
      id_user,
      email,
      password
    })
      .then(({ insertId }) => {
        if (!insertId) {
          return next(send404('could not be possible create email'));
        }

        return res.status(201).send({
          id_user: insertId,
          id_user,
          email,
          password
        });
      })
      .catch(next);
  }

  /**
   * Retrieves only an email by it's id
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static getOneEmail(req, res, next) {
    const {
      params: { id }
    } = req;

    Emails.find(id)
      .then(email => {
        if (!email.id_user) {
          return next(send404('The email does not exists in the database'));
        }

        return res.send(email);
      })
      .catch(next);
  }

  /**
   * Modify the complete email's data that have the id given
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static putEmail(req, res, next) {
    const {
      body: { id_user, email, password }
    } = req;
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    Emails.updateById(id, {
      id_user: id,
      id_user,
      email,
      password
    })
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('There user data could not be updated'));
        }

        return res.send({
          id_user: id,
          id_user,
          email,
          password
        });
      })
      .catch(next);
  }

  /**
   * Update the information of one email using it's id,
   * with this route can modify only one field if neccessary
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static patchEmail(req, res, next) {
    const { body } = req;
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to update'));
    }

    const fields = body;

    Emails.updateById(id, fields)
      .then(({ changedRows }) => {
        if (changedRows === 0) {
          return next(send404('The email data could not be updated'));
        }

        return res.send({
          ...fields
        });
      })
      .catch(next);
  }

  /**
   * Delete one email according to his id,
   * and then it its retrieved only to verify that the delete was exits
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  static deleteEmail(req, res, next) {
    const {
      params: { id }
    } = req;

    if (!id) {
      return next(send422('There are no an id provided to delete'));
    }

    Emails.deleteById(id)
      .then(({ changedRows }) => {
        if (changedRows !== 0) {
          return next(send404('There email data could not be deleted'));
        }

        return res.send({
          message: 'ok'
        });
      })
      .catch(next);
  }
};
