const { send422, invalidField } = require('../utils');
/**
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validSeatBody = (req, res, next) => {
  const {
    body: { seats }
  } = req;
  if (!seats) {
    return next(invalidField('seats'));
  }

  if (seats < 1) {
    return next(send422('invalid seats field, should not be less than 1'));
  }
  return next();
};
