const { send401 } = require('../utils');

/**
 * Validate if in the request headers are the token from the sessions to see if send information
 * o an error
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.authorization = (req, res, next) => {
  if (!req.headers.token) {
    return next(send401('Authorization Required'));
  }
  return next();
};
