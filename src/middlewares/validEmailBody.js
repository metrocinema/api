const { validEmail, invalidField } = require('../utils');

/**
 * Check if the request's body have all the necessary attributes to continue the operation
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validEmailBody = (req, res, next) => {
  const {
    body: { email, password }
  } = req;

  if (!validEmail(email)) {
    return next(invalidField('email'));
  }

  if (!password) {
    return next(invalidField('password'));
  }

  return next();
};
