/**
 * Methdo triggered when an error ocurs in the program execution
 * first show the error message in the console,
 * late if there isn't a error code set,
 * it means that the error was internally from the server and it should be a 500 status code
 * in other way, the status code from the error and the error message are send in response.
 * @param {{statusCode: number, name: 'Error', message: string, code: string}} err
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.errorHandler = (err, req, res, next) => {
  const { statusCode, message } = err;
  console.error('Error', message);
  return res
    .status(statusCode)
    .send({ ...err, statusCode: !statusCode ? 500 : statusCode });
};
