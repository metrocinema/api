const { validEmail, validCreditCard, invalidField } = require('../utils');

/**
 * Validates it the ticket detail body request contains the valid fields to do the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validTicketDetailBody = (req, res, next) => {
  const {
    body: { card_number, email }
  } = req;

  if (!validCreditCard(card_number)) {
    return next(invalidField('card_number'));
  }

  if (!validEmail(email)) {
    return next(invalidField('email'));
  }

  return next();
};
