const { withoutNumbers, invalidField } = require('../utils');

/**
 * Check if the request's body have all the necessary attributes to continue the operation
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validUserBody = (req, res, next) => {
  const {
    body: { first_name, last_name, user_type, active }
  } = req;

  if (!withoutNumbers(first_name)) {
    return next(invalidField('first_name'));
  }

  if (!withoutNumbers(last_name)) {
    return next(invalidField('last_name'));
  }

  if (!user_type) {
    return next(invalidField('user_type'));
  }

  return next();
};
