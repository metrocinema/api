const { validTime, validPrice, invalidField } = require('../utils');

/**
 * Validates it the function's body request contains the valid fields to do the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validFunctionBody = (req, res, next) => {
  const {
    body: { movie_schedule, start_date, finish_date, price, active }
  } = req;

  if (!validTime(movie_schedule)) {
    return next(invalidField('movie_schedule'));
  }

  if (!start_date) {
    return next(invalidField('start_date'));
  }

  if (!finish_date) {
    return next(invalidField('finish_date'));
  }

  if (!validPrice(price)) {
    return next(invalidField('price'));
  }

  return next();
};
