const { invalidField, send422 } = require('../utils');

/**
 * Validates it the room's body request contains the valid fields to do the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validRoomBody = (req, res, next) => {
  const {
    body: { type, capacity, active }
  } = req;

  if (!type) {
    return next(invalidField('type'));
  }

  if (!capacity) {
    return next(invalidField('capacity'));
  }

  if (capacity < 10) {
    return next(send422('invalid capacity field, should not be less than 10'));
  }

  next();
};
