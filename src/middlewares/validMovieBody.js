const { invalidField } = require('../utils');

/**
 * Validates it the movie's body request contains the valid fields to do the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validMovieBody = (req, res, next) => {
  const {
    body: {
      name,
      country,
      year,
      clasification,
      duration,
      gender,
      sinopsys,
      url_trailer,
      url_cover,
      active
    }
  } = req;

  if (!name) {
    return next(invalidField('name'));
  }

  if (!country) {
    return next(invalidField('country'));
  }

  const actualYear = new Date().getFullYear();
  if (actualYear != year) {
    return next(invalidField('year'));
  }

  if (!clasification) {
    return next(invalidField('clasification'));
  }

  if (!duration) {
    return next(invalidField('duration'));
  }

  if (!gender) {
    return next(invalidField('gender'));
  }

  if (!sinopsys) {
    return next(invalidField('sinopsys'));
  }

  if (!url_trailer) {
    return next(invalidField('url_trailer'));
  }

  if (!url_cover) {
    return next(invalidField('url_cover'));
  }

  return next();
};
