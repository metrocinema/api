const { asyncHandler } = require('./asyncHandler');
const { authorization } = require('./authorization');
const { bodyEmpty } = require('./bodyEmpty');
const { validUserBody } = require('./validUserBody');
const { validActorBody } = require('./validActorBody');
const { validDirectorBody } = require('./validDirectorBody');
const { validMovieBody } = require('./validMovieBody');
const { validRoomBody } = require('./validRoomBody');
const { validFunctionBody } = require('./validFunctionBody');
const { validSeatBody } = require('./validSeatBody');
const { validTicketBody } = require('./validTicketBody');
const { validTicketDetailBody } = require('./validTicketDetailBody');
const { validEmailBody } = require('./validEmailBody');
const { errorHandler } = require('./errorHandler');
const { userRoleAuth } = require('./userRoleAuth');
const { enableCors } = require('./enableCors');

module.exports = {
  asyncHandler,
  authorization,
  bodyEmpty,
  validUserBody,
  validActorBody,
  validDirectorBody,
  validMovieBody,
  validRoomBody,
  validFunctionBody,
  validSeatBody,
  validTicketBody,
  validTicketDetailBody,
  validEmailBody,
  errorHandler,
  userRoleAuth,
  enableCors
};
