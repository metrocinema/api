const { withoutNumbers, invalidField } = require('../utils');

/**
 * Validates if the request's body contains thes necessary to continue the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validDirectorBody = (req, res, next) => {
  const {
    body: { first_name, last_name }
  } = req;

  if (!withoutNumbers(first_name)) {
    return next(invalidField('first_name'));
  }

  if (!withoutNumbers(last_name)) {
    return next(invalidField('last_name'));
  }

  return next();
};
