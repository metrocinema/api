const { send403 } = require('../utils');

/**
 * The user's type to ve evaluated accord to the given,
 * the planned at the moment are:
 * - ADMIN
 * - SELLER
 * - CLIENT
 * - DEFAULT
 * @param { { userType: { method: Array<RegExp> } } } routes
 * object to evaluate if will go to grant access to the request
 * @returns {function(req Request, res Response, next NextFunction) : Response}
 */
exports.userRoleAuth = routes => (req, res, next) => {
  const {
    headers: { user_type },
    method,
    url
  } = req;
  const role = !user_type ? 0 : user_type;

  const roleMethods = routes[role];

  if (!roleMethods) {
    return next(send403('forbidden, invalid user role'));
  }

  const routeArray = roleMethods[method];

  if (!routeArray) {
    return next(
      send403('forbbidden, user do not have access to that method request')
    );
  }

  const canAccess = routeArray.some(route => route.test(url));

  if (!canAccess) {
    return next(send403('forbidden, user do not have access'));
  }

  return next();
};
