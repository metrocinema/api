const { send422 } = require('../utils');
/**
 * Validates if the request's body is empty, only in case that the method's request needs it:
 * 'POST' or 'PUT' or 'PATCH'
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.bodyEmpty = (req, res, next) => {
  if (req.method === 'GET' || req.method === 'DELETE') {
    return next();
  }

  if (Object.keys(req.body).length > 0) {
    return next();
  }

  return next(send422('the body of the request is empty'));
};
