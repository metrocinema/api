const { send422, invalidField } = require('../utils');

/**
 * Check if the request's body have all the necessary attributes to continue the operation
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validTicketBody = (req, res, next) => {
  const {
    body: { seats }
  } = req;

  if (!seats) {
    return next(invalidField('seats'));
  }

  if (seats < 1) {
    return next(send422('invalid seats field, should not be less than 1'));
  }

  return next();
};
