/**
 * evualuates if the given word no contains numbers
 * @param {string} word the chain to evaluate
 * @returns {boolean} the result
 */
exports.withoutNumbers = word => /^[\D.]+[\D.]$/gi.test(word);

/**
 * validates if the given email is valid according to the RFC2822 standar
 * @param {string} email the email to evaluate
 * @returns {boolean} the result
 */
exports.validEmail = email =>
  /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g.test(
    email
  );

/**
 * validates in 24hrs format examples:
 * 23:50:00
 * 14:00
 * 23:00
 * 9:30
 * 19:30
 * @param {string} time
 */
exports.validTime = time =>
  /([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?/g.test(time);

/**
 * evaluaes if the given chain is a valid credit card according to the pattern
 * @param {string} creditCard
 * @returns {boolean} the result
 */
exports.validCreditCard = creditCard =>
  /(\d{4}[-. ]?){4}|\d{4}[-. ]?\d{6}[-. ]?\d{5}/g.test(creditCard);

/**
 * evaluaes if the given chain is a valid price according to the pattern
 * @param {string} creditCard
 * @returns {boolean} the result
 */
exports.validPrice = price => /^([0-9]+(\.[0-9]*)?)$/g.test(price);
