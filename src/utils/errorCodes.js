/**
 * @param {string} msg message to include in the body of the error
 * @returns {{statusCode: number, name: 'Error', message: msg, code: string}}
 * response's body object to send in case of an error 401
 */
const send401 = msg => ({
  statusCode: 401,
  name: 'Error',
  message: msg || 'Authorization Required',
  code: 'AUTHORIZATION_REQUIRED'
});

/**
 * @param {string} msg message to include in the body of the error
 * @returns {{statusCode: number, name: 'Error', message: msg, code: string}}
 * response's body object to send in case of an error 403
 */
const send403 = msg => ({
  statusCode: 403,
  name: 'Error',
  message: msg,
  code: 'FORBIDDEN'
});

/**
 * @param {string} msg message to include in the body of the error
 * @returns {{statusCode: number, name: 'Error', message: msg, code: string}}
 * response's body object to send in case of an error 404
 */
const send404 = msg => ({
  statusCode: 404,
  name: 'Error',
  message: msg,
  code: 'NOT_FOUND'
});

/**
 * @param {string} msg message to include in the body of the error
 * @returns {{statusCode: number, name: 'Error', message: msg, code: string}}
 * response's body object to send in case of an error 422
 */
const send422 = msg => ({
  statusCode: 422,
  name: 'Error',
  message: msg,
  code: 'UNPROCESSABLE_ENTITY'
});

/**
 * @param {string} msg message to include in the body of the error
 * @returns {{statusCode: number, name: 'Error', message: msg, code: string}}
 * response's body object to send in case of an error 500
 */
const send500 = msg => ({
  statusCode: 500,
  name: 'Error',
  message: msg,
  code: 'INTERNAL_SERVER_ERROR'
});

/**
 * create a string template utility excusively
 * for the 422 error message, because it is very repetitive
 * or in case to need it in other things
 * @param {string} field the field's name to put in the template
 * @returns {{statusCode: number, name: 'Error', message: field, code: string}}
 * response's body object to send in case of an error 422 with the string generated as message
 */
const invalidField = field => send422(`invalid ${field} field`);

module.exports = {
  send401,
  send403,
  send404,
  send422,
  send500,
  invalidField
};
