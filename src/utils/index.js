const regx = require('./regexp');
const errorCodes = require('./errorCodes');

module.exports = {
  ...regx,
  ...errorCodes
};
