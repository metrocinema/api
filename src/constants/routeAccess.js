const { USERTYPES } = require('./userTypes');

const { ADMIN, CLIENT, SELLER, DEFAULT } = USERTYPES;

exports.ROUTEACCESS = {
  [ADMIN]: {
    GET: [/(\/\w+)+\/?/],
    POST: [/(\/\w+)+\/?/],
    PUT: [/(\/\w+)+\/?/],
    PATCH: [/(\/\w+)+\/?/],
    DELETE: [/(\/\w+)+\/?/]
  },
  [SELLER]: {
    GET: [/(\/\w+)+\/?/],
    POST: [/(\/\w+)+\/?/],
    PUT: [/(\/\w+)+\/?/],
    PATCH: [/(\/\w+)+\/?/],
    DELETE: [/(\/\w+)+\/?/]
  },
  [CLIENT]: {
    GET: [/(\/\w+)+\/?/],
    POST: [/(\/\w+)+\/?/],
    PUT: [/(\/\w+)+\/?/],
    PATCH: [/(\/\w+)+\/?/],
    DELETE: [/(\/\w+)+\/?/]
  },
  [DEFAULT]: {
    GET: [/(\/movies)(\/\w+)?\/?/],
    POST: [/(\/users)(\/login)?\/?/]
  }
};
