const { USERTYPES } = require('./userTypes');
const { ROUTEACCESS } = require('./routeAccess');

module.exports = {
  USERTYPES,
  ROUTEACCESS
};
