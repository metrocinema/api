const connectorDefault = require('./connector');

class Functions {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.movie_function (
      id_function INT NOT NULL AUTO_INCREMENT,
      id_movie INT NOT NULL,
      id_room INT NOT NULL,
      movie_schedule VARCHAR(45) NULL,
      start_date VARCHAR(45) NULL,
      finish_date VARCHAR(45) NULL,
      price VARCHAR(10) NULL,
      active VARCHAR(5) NULL,
      PRIMARY KEY (id_function),
      INDEX fk_Functions_Movies1_idx (id_movie ASC) VISIBLE,
      INDEX fk_Functions_Rooms1_idx (id_room ASC) VISIBLE,
      CONSTRAINT fk_Functions_Movies1
        FOREIGN KEY (id_movie)
        REFERENCES metrocinemas.movie (id_movie)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT fk_Functions_Rooms1
        FOREIGN KEY (id_room)
        REFERENCES metrocinemas.room (id_room)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;
  `;

    console.log('Preparing to create the movie function table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...movie function table created!');
        return resolve('Success');
      });
    });
  }

  static insert({
    id_movie,
    id_room,
    movie_schedule,
    start_date,
    finish_date,
    price,
    active
  }) {
    const sql = `INSERT INTO movie_function (
                  id_movie,
                  id_room,
                  movie_schedule,
                  start_date,
                  finish_date,
                  price,
                  active
                  ) VALUES (?,?,?,?,?,?,?)`;

    console.log(`Inserting movie function ${movie_schedule} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [
          id_movie,
          id_room,
          movie_schedule,
          start_date,
          finish_date,
          price,
          active
        ],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(
            `...movie function ${results.insertId} inserted into database`
          );

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM movie_function';

    const sql = !id ? baseSQL : `${baseSQL} WHERE id_function = ? LIMIT 1`;

    console.log(`Querying for movie function id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const functions = results.map(result => ({
          id_function: result.id_function,
          id_movie: result.id_movie,
          id_room: result.id_room,
          movie_schedule: result.movie_schedule,
          start_date: result.start_date,
          finish_date: result.finish_date,
          price: result.price,
          active: result.active
        }));

        if (!id) {
          return resolve(functions);
        }

        return resolve(functions[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'UPDATE movie_function SET active = 0 WHERE id_function = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE movie_function SET ${queryFields} WHERE id_function = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Functions;
