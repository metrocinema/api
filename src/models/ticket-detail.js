const connectorDefault = require('./connector');

class TicketDetail {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.ticket_detail (
      id_ticket_detail INT NOT NULL AUTO_INCREMENT,
      card_number VARCHAR(45) NULL,
      id_ticket INT NOT NULL,
      id_user INT NOT NULL,
      email VARCHAR(45) NULL,
      id_seat INT NOT NULL,
      PRIMARY KEY (id_ticket_detail),
      INDEX fk_TIcketsDetail_Tickets1_idx (id_ticket ASC) VISIBLE,
      INDEX fk_TIcketsDetail_Users1_idx (id_user ASC) VISIBLE,
      INDEX fk_TIcketsDetail_Seats1_idx (id_seat ASC) VISIBLE,
      CONSTRAINT fk_TIcketsDetail_Tickets1
        FOREIGN KEY (id_ticket)
        REFERENCES metrocinemas.ticket (id_ticket)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT fk_TIcketsDetail_Users1
        FOREIGN KEY (id_user)
        REFERENCES metrocinemas.user (id_user)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT fk_TIcketsDetail_Seats1
        FOREIGN KEY (id_seat)
        REFERENCES metrocinemas.seat (id_seat)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;
    `;

    console.log('Preparing to create the ticket detail table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('... ticket detail table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ card_number, id_ticket, id_user, email, id_seat }) {
    const sql = `
    INSERT INTO ticket_detail (
      card_number, 
      id_ticket, 
      id_user, 
      email, 
      id_seat
    ) VALUES (?, ?, ?, ?, ?)
    `;

    console.log(`Inserting ticket detail ${id_user} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [card_number, id_ticket, id_user, email, id_seat],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(
            `...ticket detail ${results.insertId} inserted into database`
          );

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM ticket_detail';

    const sql = !id ? baseSQL : `${baseSQL} WHERE id_ticket_detail = ? LIMIT 1`;

    console.log(`Querying for ticket detail id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const tickets_detail = results.map(result => ({
          id_ticket_detail: result.id_ticket_detail,
          card_number: result.card_number,
          id_ticket: result.id_ticket,
          id_user: result.id_user,
          email: result.email,
          id_seat: results.id_seat
        }));

        if (!id) {
          return resolve(tickets_detail);
        }

        return resolve(tickets_detail[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM ticket_detail WHERE id_ticket_detail = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE ticket_detail SET ${queryFields} WHERE id_ticket_detail = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = TicketDetail;
