const connectorDefault = require('./connector');

class Actors {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.actor (
      id_actor INT NOT NULL AUTO_INCREMENT,
      first_name VARCHAR(45) NULL,
      last_name VARCHAR(45) NULL,
      PRIMARY KEY (id_actor))
    ENGINE = InnoDB;`;

    console.log('Preparing to create the actors table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...actor table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ first_name, last_name }) {
    const sql = `INSERT INTO actor (
                first_name,
                last_name
                ) VALUES (?, ?)`;

    console.log(`Inserting actor ${first_name} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [first_name, last_name],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...actor ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM actor';
    const sql = !id ? baseSQL : `${baseSQL} WHERE id_actor = ? LIMIT 1`;

    console.log(`Querying for actor id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const actors = results.map(result => ({
          id_actor: result.id_actor,
          first_name: result.first_name,
          last_name: result.last_name
        }));

        if (!id) {
          return resolve(actors);
        }

        console.log('actors[0] ', actors[0]);
        return resolve(actors[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM actor WHERE id_actor = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE actor SET ${queryFields} WHERE id_actor = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Actors;
