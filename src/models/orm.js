// Case Conversion Functions
function camelCaseToUpperCase(str) {
  return str.replace(/([a-z])([A-Z])/, "$1_$2").toUpperCase();
}

function camelCaseToLowerCase(str) {
  return str.replace(/([a-z])([A-Z])/, "$1_$2").toLowerCase();
}

function camelToTitle(str, delimiter) {
  return str
    .replace(/([A-Z][a-z]+)/g, " $1") // Words beginning with UC
    .replace(/([A-Z][A-Z]+)/g, " $1") // "Words" of only UC
    .replace(/([^A-Za-z ]+)/g, " $1") // "Words" of non-letters
    .trim() // Remove any leading/trailing spaces
    .replace(/[ ]/g, delimiter || " "); // Replace all spaces with the delim
}

class Test {
  message = "hii";
  static getInstance() {
    console.log(this.instance);
    if (!this.instance) {
      this.instance = new Test();
    }
    return this.instance;
  }

  static setInstance(instance) {
    if (!this.instance) {
      this.instance = new Test();
    }
    this.instance[instance] = camelCaseToLowerCase(instance);
  }

  static get models() {
    console.log("wiii", this.man);
    return this.instance;
  }
}

Test.man = { nombre: "erick", alo: "hola" };

Test.setInstance("AlaLo");
Test.setInstance("oraleChido");
Test.setInstance("WaliEva");
Test.setInstance("maloBueno");
Test.setInstance("Saluenfermedad");
const algo = Test.getInstance();
const alo = Test.models;
const { AlaLo, oraleChido, WaliEva, maloBueno, Saluenfermedad } = Test.models;

console.log(algo);
console.dir(alo);

console.log(AlaLo);
console.log(oraleChido);
console.log(WaliEva);
console.log(maloBueno);
console.log(Saluenfermedad);

console.log(Object.keys(Test.models).sort());
console.log([AlaLo, oraleChido, WaliEva, maloBueno, Saluenfermedad].sort());

console.log(Test.man);

function camelCaseToUpperCase(str) {
  return str.replace(/([a-z])([A-Z])/, "$&_$&").toLowerCase();
}

console.log(camelCaseToUpperCase("AlaLo"));

function styleHyphenFormat(propertyName) {
  return propertyName.replace(/([A-Z][a-z])+/g, "_" + "$&").toLowerCase();
}

console.log(styleHyphenFormat("borderTopLoco"));

console.log(
  "borderTopLoco".match(
    /[a-z]+|[0-9]+|(?:[A-Z][a-z]+)|(?:[A-Z]+(?=(?:[A-Z][a-z])|[^AZa-z]|[$\d\n]))/g
  )
);

////////////////////////////////////////////////////////////////////////////////////
// function camelCaseToUpperCase(str) {
//     return str.replace(/([a-z])([A-Z])/, "$&_$&").toLowerCase();
// }

// console.log(camelCaseToUpperCase('AlaLo'))

// function styleHyphenFormat(propertyName) {
//     return propertyName.replace(/([A-Z][a-z])+/g, "_" + "$&").toLowerCase();
// }

// console.log(styleHyphenFormat("borderTopLoco"))

// console.log("borderTopLoco".match(/[a-z]+|[0-9]+|(?:[A-Z][a-z]+)|(?:[A-Z]+(?=(?:[A-Z][a-z])|[^AZa-z]|[$\d\n]))/g))

////////////////////////////////////////////////////////////////////////////////////
// proto medio chido
// function camelCaseToUpperCase(str) {
//     return str.replace(/([a-z])([A-Z])/, "$&_$&").toLowerCase();
// }

// console.log(camelCaseToUpperCase('AlaLo'))

// function styleHyphenFormat(propertyName) {
//     return propertyName.replace(/([A-Z])+/g, "_" + "$&").toLowerCase();
// }

// console.log(styleHyphenFormat("borderTopLocoLocoLocoLocoLeanoGarciaDeLaOcaLOCA"))

// console.log("borderTopLoco".match(/[a-z]+|[0-9]+|(?:[A-Z][a-z]+)|(?:[A-Z]+(?=(?:[A-Z][a-z])|[^AZa-z]|[$\d\n]))/g))

////////////////////////////////////////////////////////////////////////////////////
// proto super chido

// function camelCaseToUpperCase(str) {
//     return str.replace(/([a-z])([A-Z])/, "$&_$&").toLowerCase();
// }

// console.log(camelCaseToUpperCase('AlaLo'))

// function styleHyphenFormat(propertyName) {
//     return propertyName.replace(/([A-Z])/g, "_$&").toLowerCase();
// }

// console.log(styleHyphenFormat("borderTopLocoLocoLocoLocoLeanoGarciaDeLaOcaLOCA"))

// console.log("borderTopLoco".match(/[a-z]+|[0-9]+|(?:[A-Z][a-z]+)|(?:[A-Z]+(?=(?:[A-Z][a-z])|[^AZa-z]|[$\d\n]))/g))

////////////////////////////////////////////////////////////////////////////////////
// para cuando se necesiten hacer con funciones
// function styleHyphenFormat(propertyName) {
//     function upperToHyphenLower(match) {
//         return '-' + match.toLowerCase();
//     }
//     return propertyName.replace(/[A-Z]/, upperToHyphenLower);
// }
