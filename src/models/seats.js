const connectorDefault = require('./connector');

class Seats {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.seat (
      id_seat INT NOT NULL AUTO_INCREMENT,
      seats VARCHAR(45) NULL,
      id_room INT NOT NULL,
      id_function INT NOT NULL,
      PRIMARY KEY (id_seat),
      INDEX fk_Seats_Rooms1_idx (id_room ASC) VISIBLE,
      INDEX fk_Seats_Functions1_idx (id_function ASC) VISIBLE,
      CONSTRAINT fk_Seats_Rooms1
        FOREIGN KEY (id_room)
        REFERENCES metrocinemas.room (id_room)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT fk_Seats_Functions1
        FOREIGN KEY (id_function)
        REFERENCES metrocinemas.movie_function (id_function)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;`;

    console.log('Preparing to create the seat table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...seat table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ seats, id_room, id_function }) {
    const sql = `INSERT INTO seat
                (
                  seats,
                  id_room,
                  id_function
                ) VALUES (?,?,?)
        `;

    console.log(`Inserting seat ${seats} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [seats, id_room, id_function],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...seat ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM seat';

    const sql = !id ? baseSQL : `${baseSQL} WHERE id_seat = ? LIMIT 1`;

    console.log(`Querying for seat id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const seats = results.map(result => ({
          id_seat: result.id_seat,
          seats: result.seats,
          id_room: result.id_room,
          id_function: result.id_function
        }));

        if (!id) {
          return resolve(seats);
        }

        return resolve(seats[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM seat WHERE id_seat = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        return resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE seat SET ${queryFields} WHERE id_seat = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Seats;
