const connectorDefault = require('./connector');

class Users {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.user (
      id_user INT AUTO_INCREMENT,
      first_name VARCHAR(20) NULL,
      last_name VARCHAR(20) NULL,
      user_type VARCHAR(5) NULL,
      active VARCHAR(5) NULL,
      PRIMARY KEY (id_user))
    ENGINE = InnoDB;`;

    console.log('Preparing to create the users table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...users table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ first_name, last_name, user_type, active }) {
    const sql = `
    INSERT INTO user (
      first_name,
      last_name,
      user_type,
      active
    ) VALUES (?, ?, ?, ?)
    `;

    console.log(`Inserting user ${first_name} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [first_name, last_name, user_type || 1, active || 1],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...user ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM user';

    const sql = !id ? baseSQL : `${baseSQL} WHERE id_user = ? LIMIT 1`;

    console.log(`Querying for user id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const users = results.map(result => ({
          id_user: result.id_user,
          first_name: result.first_name,
          last_name: result.last_name,
          user_type: result.user_type,
          active: result.active
        }));

        if (!id) {
          return resolve(users);
        }

        return resolve(users[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'UPDATE user SET active = 0 WHERE id_user = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE user SET ${queryFields} WHERE id_user = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Users;
