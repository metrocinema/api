const connectorDefault = require('./connector');

class Tickets {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.ticket (
      id_ticket INT NOT NULL AUTO_INCREMENT,
      id_movie INT NOT NULL,
      id_room INT NOT NULL,
      id_function INT NOT NULL,
      seats VARCHAR(45) NULL,
      PRIMARY KEY (id_ticket),
      INDEX fk_Tickets_Movies1_idx (id_movie ASC) VISIBLE,
      INDEX fk_Tickets_Rooms1_idx (id_room ASC) VISIBLE,
      INDEX fk_Tickets_Functions1_idx (id_function ASC) VISIBLE,
      CONSTRAINT fk_Tickets_Movies1
        FOREIGN KEY (id_movie)
        REFERENCES metrocinemas.movie (id_movie)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT fk_Tickets_Rooms1
        FOREIGN KEY (id_room)
        REFERENCES metrocinemas.room (id_room)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT fk_Tickets_Functions1
        FOREIGN KEY (id_function)
        REFERENCES metrocinemas.movie_function (id_function)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;`;

    console.log('Preparing to create the ticket table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...ticket table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ id_movie, id_room, id_function, seats }) {
    const sql = `INSERT INTO ticket (
                id_movie,
                id_room,
                id_function,
                seats
                ) VALUES (?,?,?,?);`;

    console.log(`Inserting ticket ${seats} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [id_movie, id_room, id_function, seats],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...ticket ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM ticket';

    const sql = !id ? baseSQL : `${baseSQL} WHERE  id_ticket = ? LIMIT 1`;

    console.log(`Querying for ticket id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const tickets = results.map(result => ({
          id_ticket: result.id_ticket,
          id_movie: result.id_movie,
          id_room: result.id_room,
          id_function: result.id_function,
          seats: result.seats
        }));

        if (!id) {
          return resolve(tickets);
        }

        return resolve(tickets[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM ticket WHERE id_ticket = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE ticket SET ${queryFields} WHERE id_ticket = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Tickets;
