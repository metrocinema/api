const connectorDefault = require('./connector');

class Rooms {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.room (
      id_room INT NOT NULL AUTO_INCREMENT,
      type VARCHAR(45) NULL,
      capacity VARCHAR(45) NULL,
      active VARCHAR(5) NULL,
      PRIMARY KEY (id_room))
    ENGINE = InnoDB;`;

    console.log('Preparing to create the room table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...room table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ type, capacity, active }) {
    const sql = `INSERT INTO room (
                type,
                capacity,
                active
                ) VALUES (?, ?, ?)`;

    console.log(`Inserting room ${type} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [type, capacity, active || 1],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...room ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM room WHERE active = 1';

    const sql = !id ? baseSQL : `${baseSQL} AND id_room = ? LIMIT 1`;

    console.log(`Querying for room id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const rooms = results.map(result => ({
          id_room: result.id_room,
          type: result.type,
          capacity: result.capacity,
          active: result.active
        }));

        if (!id) {
          return resolve(rooms);
        }

        return resolve(rooms[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'UPDATE room SET active = 0 WHERE id_room = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE room SET ${queryFields} WHERE id_room = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Rooms;
