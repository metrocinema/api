const connectorDefault = require('./connector');

class Movies {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.movie (
      id_movie INT NOT NULL AUTO_INCREMENT,
      name VARCHAR(20) NULL,
      country VARCHAR(20) NULL,
      year VARCHAR(10) NULL,
      clasification VARCHAR(10) NULL,
      duration VARCHAR(10) NULL,
      gender VARCHAR(10) NULL,
      sinopsys VARCHAR(200) NULL,
      id_actor INT NOT NULL,
      id_director INT NOT NULL,
      url_trailer VARCHAR(150) NULL,
      url_cover VARCHAR(150) NULL,
      active VARCHAR(5) NULL,
      PRIMARY KEY (id_movie),
      INDEX fk_Movies_Actors_idx (id_actor ASC) VISIBLE,
      INDEX fk_Movies_Directors1_idx (id_director ASC) VISIBLE,
      CONSTRAINT fk_Movies_Actors
        FOREIGN KEY (id_actor)
        REFERENCES metrocinemas.actor (id_actor)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT fk_Movies_Directors1
        FOREIGN KEY (id_director)
        REFERENCES metrocinemas.director (id_director)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;`;

    console.log('Preparing to create the movie table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...movie table created!');
        return resolve('Success');
      });
    });
  }

  static insert({
    name,
    country,
    year,
    clasification,
    duration,
    gender,
    sinopsys,
    id_actor,
    id_director,
    url_trailer,
    url_cover,
    active
  }) {
    const sql = `INSERT INTO movie (
                name,
                country,
                year,
                clasification,
                duration,
                gender,
                sinopsys,
                id_actor,
                id_director,
                url_trailer,
                url_cover,
                active
                ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`;

    console.log(`Inserting movie ${name} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [
          name,
          country,
          year,
          clasification,
          duration,
          gender,
          sinopsys,
          id_actor,
          id_director,
          url_trailer,
          url_cover,
          active
        ],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...movie ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM movie WHERE active = 1';

    const sql = !id ? baseSQL : `${baseSQL} AND id_movie = ? LIMIT 1`;

    console.log(`Querying for movie id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const movies = results.map(result => ({
          id_movie: result.id_movie,
          name: result.name,
          country: result.country,
          year: result.year,
          clasification: result.clasification,
          duration: result.duration,
          gender: result.gender,
          sinopsys: result.sinopsys,
          id_actor: result.id_actor,
          id_director: result.id_director,
          url_trailer: result.urlTrailer,
          url_cover: result.urlCover,
          active: result.active
        }));

        if (!id) {
          return resolve(movies);
        }

        return resolve(movies[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'UPDATE movie SET active = 0 WHERE id_movie = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE movie SET ${queryFields} WHERE id_movie = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Movies;
