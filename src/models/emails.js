const connectorDefault = require('./connector');

class Emails {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS metrocinemas.email (
      id_user INT NOT NULL,
      email VARCHAR(20) NULL,
      password VARCHAR(20) NULL,
      INDEX fk_email_user1_idx (id_user ASC) VISIBLE,
      CONSTRAINT fk_email_user1
        FOREIGN KEY (id_user)
        REFERENCES metrocinemas.user (id_user)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;`;

    console.log('Preparing to create the email table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...email table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ id_user, email, password }) {
    const sql = `INSERT INTO email (
                id_user,
                email,
                password
                ) VALUES (?, ?, ?)`;

    console.log(`Inserting email ${email} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [id_user, email, password],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...email ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM email';

    const sql = !id ? baseSQL : `${baseSQL} WHERE id_user = ? LIMIT 1`;

    console.log(`Querying for email id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const emails = results.map(result => ({
          id_user: result.id_user,
          email: result.email,
          password: result.password
        }));

        if (!id) {
          return resolve(emails);
        }

        return resolve(emails[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM email WHERE id_email = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE email SET ${queryFields} WHERE id_user = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Emails;
