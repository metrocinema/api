const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const path = require('path');
const apiRouter = require('./routes');
const { errorHandler, userRoleAuth, enableCors } = require('./middlewares');
const { ROUTEACCESS } = require('./constants');

dotenv.load({
  path: path.resolve(process.cwd(), '.env')
});

const app = express();
const {
  env: { PORT }
} = process;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(enableCors);
app.use('/api', userRoleAuth({ ...ROUTEACCESS }));
app.use('/api', apiRouter);
app.use('/api', errorHandler);

app.listen(PORT, () => console.log(`Server listen on port ${PORT}`));
