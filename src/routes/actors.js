const { Router } = require('express');
const { ActorsController } = require('../controllers');
const { validActorBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, ActorsController.getAllActors)
  .get('/:id', authorization, ActorsController.getOneActor)
  .post('/', bodyEmpty, validActorBody, ActorsController.postActor)
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validActorBody,
    ActorsController.putActor
  )
  .patch('/:id', authorization, bodyEmpty, ActorsController.patchActor)
  .delete('/:id', authorization, ActorsController.deleteActor);

module.exports = router;
