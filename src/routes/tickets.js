const { Router } = require('express');
const { TicketsController } = require('../controllers');
const { validTicketBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, TicketsController.getAllTickets)
  .get('/:id', authorization, TicketsController.getOneTicket)
  .post('/', bodyEmpty, validTicketBody, TicketsController.postTicket)
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validTicketBody,
    TicketsController.putTicket
  )
  .patch('/:id', authorization, bodyEmpty, TicketsController.patchTicket)
  .delete('/:id', authorization, TicketsController.deleteTicket);

module.exports = router;
