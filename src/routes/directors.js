const { Router } = require('express');
const { DirectorsController } = require('../controllers');
const {
  validDirectorBody,
  bodyEmpty,
  authorization
} = require('../middlewares');

const router = Router();

router
  .get('/', authorization, DirectorsController.getAllDirectors)
  .get('/:id', authorization, DirectorsController.getOneDirector)
  .post('/', bodyEmpty, validDirectorBody, DirectorsController.postDirector)
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validDirectorBody,
    DirectorsController.putDirector
  )
  .patch('/:id', authorization, bodyEmpty, DirectorsController.patchDirector)
  .delete('/:id', authorization, DirectorsController.deleteDirector);

module.exports = router;
