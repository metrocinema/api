const { Router } = require('express');
const { MoviesController } = require('../controllers');
const { validMovieBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, MoviesController.getAllMovies)
  .get('/:id', authorization, bodyEmpty, validMovieBody, MoviesController.getOneMovie)
  .post('/', authorization, MoviesController.postMovie)
  .put('/:id', authorization, bodyEmpty, validMovieBody, MoviesController.putMovie)
  .patch('/:id', authorization, bodyEmpty, MoviesController.patchMovie)
  .delete('/:id', authorization, bodyEmpty, MoviesController.deleteMovie);

module.exports = router;
