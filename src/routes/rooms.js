const { Router } = require('express');
const { RoomsController } = require('../controllers');
const { validRoomBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, RoomsController.getAllRooms)
  .get('/:id', authorization, RoomsController.getOneRoom)
  .post('/', bodyEmpty, validRoomBody, RoomsController.postRoom)
  .put('/:id', authorization, bodyEmpty, validRoomBody, RoomsController.putRoom)
  .patch('/:id', authorization, bodyEmpty, RoomsController.patchRoom)
  .delete('/:id', authorization, RoomsController.deleteRoom);

module.exports = router;
