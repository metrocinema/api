const { Router } = require('express');
const { TicketDetailController } = require('../controllers');
const {
  validTicketDetailBody,
  bodyEmpty,
  authorization
} = require('../middlewares');

const router = Router();

router
  .get('/', authorization, TicketDetailController.getAllTicketDetail)
  .get('/:id', authorization, TicketDetailController.getOneTicketDetail)
  .post(
    '/',
    bodyEmpty,
    validTicketDetailBody,
    TicketDetailController.postTicketDetail
  )
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validTicketDetailBody,
    TicketDetailController.putTicketDetail
  )
  .patch(
    '/:id',
    authorization,
    bodyEmpty,
    TicketDetailController.patchTicketDetail
  )
  .delete('/:id', authorization, TicketDetailController.deleteTicketDetail);

module.exports = router;
