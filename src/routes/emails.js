const { Router } = require('express');
const { EmailsController } = require('../controllers');
const { validEmailBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, EmailsController.getAllEmails)
  .get('/:id', authorization, EmailsController.getOneEmail)
  .post(
    '/',
    authorization,
    bodyEmpty,
    validEmailBody,
    EmailsController.postEmail
  )
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validEmailBody,
    EmailsController.putEmail
  )
  .patch('/:id', authorization, EmailsController.patchEmail)
  .delete('/:id', authorization, EmailsController.deleteEmail);

module.exports = router;
