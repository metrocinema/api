const { Router } = require('express');
const { SeatsController } = require('../controllers');
const { validSeatBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, SeatsController.getAllSeats)
  .get('/:id', authorization, SeatsController.getOneSeat)
  .post('/', bodyEmpty, validSeatBody, SeatsController.postSeat)
  .put('/:id', authorization, bodyEmpty, validSeatBody, SeatsController.putSeat)
  .patch('/:id', authorization, bodyEmpty, SeatsController.patchSeat)
  .delete('/:id', authorization, SeatsController.deleteSeat);

module.exports = router;
