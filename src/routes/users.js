const { Router } = require('express');
const { UsersController } = require('../controllers');
const { validUserBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, UsersController.getAllUsers)
  .post('/', bodyEmpty, validUserBody, UsersController.postUSer)
  .get('/logout', authorization, UsersController.logoutUser)
  .post('/login', bodyEmpty, UsersController.loginUser)
  .get('/:id', authorization, UsersController.getUser)
  .put('/:id', authorization, bodyEmpty, validUserBody, UsersController.putUser)
  .patch('/:id', authorization, bodyEmpty, UsersController.patchUser)
  .delete('/:id', authorization, UsersController.deleteUser);

module.exports = router;
