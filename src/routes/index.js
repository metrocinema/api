const { Router } = require('express');

const actorsRouter = require('./actors');
const seatsRouter = require('./seats');
const moviesRouter = require('./movies');
const directorsRouter = require('./directors');
const roomsRouter = require('./rooms');
const functionsRouter = require('./functions');
const ticketsRouter = require('./tickets');
const usersRouter = require('./users');
const ticketsDetailRouter = require('./ticket-detail');
const emailsRouter = require('./emails');

const router = Router();

router.use('/actors', actorsRouter);
router.use('/seats', seatsRouter);
router.use('/movies', moviesRouter);
router.use('/directors', directorsRouter);
router.use('/rooms', roomsRouter);
router.use('/functions', functionsRouter);
router.use('/tickets', ticketsRouter);
router.use('/users', usersRouter);
router.use('/ticket-detail', ticketsDetailRouter);
router.use('/emails', emailsRouter);

module.exports = router;
