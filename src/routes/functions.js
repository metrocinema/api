const { Router } = require('express');
const { FunctionsController } = require('../controllers');
const {
  validFunctionBody,
  bodyEmpty,
  authorization
} = require('../middlewares');

const router = Router();

router
  .get('/', authorization, FunctionsController.getAllFunctions)
  .post('/', bodyEmpty, validFunctionBody, FunctionsController.postFunction)
  .get('/:id', authorization, FunctionsController.getOneFunction)
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validFunctionBody,
    FunctionsController.putFunction
  )
  .patch('/:id', authorization, bodyEmpty, FunctionsController.patchFunction)
  .delete('/:id', authorization, FunctionsController.deleteFunction);

module.exports = router;
