const { it, describe } = require('mocha');
const { expect, should, assert } = require('chai');

describe('#Testing a hello world as a first test', () => {
  it('should say hello world', () => {
    expect('hola mundo').to.be.equals('hola mundo');
  });
  it('should be a number', () => {
    expect(1).to.be.a('number');
  });
});
