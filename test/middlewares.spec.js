const { it, describe, before, after } = require('mocha');
const { expect, should, assert } = require('chai');
const supertest = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const { USERTYPES } = require('../src/constants');
const {
  asyncHandler,
  authorization,
  bodyEmpty,
  errorHandler,
  validUserBody,
  validActorBody,
  validDirectorBody,
  validEmailBody,
  validFunctionBody,
  validRoomBody,
  validSeatBody,
  // validTicketBody,
  validTicketDetailBody,
  userRoleAuth,
  enableCors
} = require('../src/middlewares');
const { send404 } = require('../src/utils');

describe('#Testing functionality of the middlewares', () => {
  let server;
  let request;
  let delay;
  before(() => {
    server = express();
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: false }));

    request = supertest(server);

    delay = time => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(Date());
        }, time);
      });
    };
  });

  describe('- Testing the async handler', () => {
    before(() => {
      server.get(
        '/',
        asyncHandler(async (req, res) => {
          const conectioTime = await delay(100);
          res.send(`${conectioTime} yolo`);
        })
      );
    });
    it('should works like a normal endpoints worflow', done => {
      request
        .get('/')
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });
  });

  describe('- Testing the auth handler', () => {
    before(() => {
      server.get(
        '/auth',
        authorization,
        (req, res, next) => {
          try {
            res.status(200).send(Date());
          } catch (error) {
            console.error('aaaa', error);
          }
        },
        errorHandler
      );
    });

    it('should reject the request sending a 401 error status', done => {
      request
        .get('/auth')
        .expect(401)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }

          expect(text).to.contain('Authorization Required');
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should pass to the endpoint and give a status 200', done => {
      request
        .get('/auth')
        .set('token', 'Hola')
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });
  });

  describe('- Testing bodyEmpty Middleware', () => {
    before(() => {
      server.get(
        '/get',
        bodyEmpty,
        (req, res) => res.send(Date()),
        errorHandler
      );
      server.post(
        '/',
        bodyEmpty,
        (req, res) => res.status(201).send(req.body),
        errorHandler
      );
    });

    it('should send an 201', done => {
      request
        .post('/')
        .send({
          Hi: 'hola'
        })
        .expect(201)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send an 422 error in POST method because request body is empty', done => {
      request
        .post('/')
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }

          const res = JSON.parse(text);

          expect(res.message).to.be.equal('the body of the request is empty');
          expect(res.name).to.be.equal('Error');
          done();
        });
    });

    it('should send a 200 status in GET method', done => {
      request
        .get('/get')
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });
  });

  describe('- Testing error handler middleware', () => {
    before(() => {
      server.get('/error/:id', (req, res, next) => {
        const result = parseInt(req.params.id);
        if (isNaN(result)) {
          return next(send404('An error occur'));
        }
        res.send('all was ok');
      });
      server.use(errorHandler);
    });

    it('should obtain an error from the errorHanlder because the param id was not a number', done => {
      request
        .get('/error/alo')
        .expect(404)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('An error occur');
          done();
        });
    });

    it('should not obtain an error from the request', done => {
      request
        .get('/error/1')
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('all was ok');
          done();
        });
    });
  });

  describe('- Testing validUserBody middleware', () => {
    before(() => {
      server.post(
        '/users',
        validUserBody,
        (req, res, next) => res.send('creado usuario con exito'),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          user_type: 1,
          active: true
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('creado usuario con exito');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: 'kj32jk',
          last_name: 'Fulano',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is empty', done => {
      request
        .post('/users')
        .send({
          first_name: '',
          last_name: 'Fulano',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'kjk21j',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is empty', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: '',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });

    it('should send a 422 status because the body fiel user_type is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid user_type field');
          done();
        });
    });

    it('should send a 200 status inclusively if the body field active is not there', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          user_type: 1
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.not.contain('invalid');
          expect(text).to.contain('creado usuario con exito');
          done();
        });
    });
  });

  describe('- Testing validActorBody middleware', () => {
    before(() => {
      server.post(
        '/actors',
        validActorBody,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/actors')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano'
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is incorrect', done => {
      request
        .post('/actors')
        .send({
          first_name: 'j3knej23n',
          last_name: 'Fulano'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is empty', done => {
      request
        .post('/actors')
        .send({
          first_name: '',
          last_name: 'Fulano'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is incorrect', done => {
      request
        .post('/actors')
        .send({
          first_name: 'Fulanito',
          last_name: 'djk232'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is empty', done => {
      request
        .post('/actors')
        .send({
          first_name: 'Fulanito',
          last_name: ''
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });
  });

  describe('- Testing validDirectorBody middleware', () => {
    before(() => {
      server.post(
        '/directors',
        validDirectorBody,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/directors')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano'
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is incorrect', done => {
      request
        .post('/directors')
        .send({
          first_name: 'kj2k2j',
          last_name: 'Fulano'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is empty', done => {
      request
        .post('/directors')
        .send({
          first_name: '',
          last_name: 'Fulano'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is incorrect', done => {
      request
        .post('/directors')
        .send({
          first_name: 'Fulanito',
          last_name: 'ji3j34'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is empty', done => {
      request
        .post('/directors')
        .send({
          first_name: 'Fulanito',
          last_name: ''
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });
  });

  describe('- Testing validEmailBody middleware', () => {
    before(() => {
      server.post(
        '/emails',
        validEmailBody,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/emails')
        .send({
          email: 'Fulanito@correo.com',
          password: '1234'
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field email is incorrect', done => {
      request
        .post('/emails')
        .send({
          email: 'Fulanito',
          password: '1234'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid email field');
          done();
        });
    });

    it('should send a 422 status because the body field email is empty', done => {
      request
        .post('/emails')
        .send({
          email: '',
          password: '1234'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid email field');
          done();
        });
    });

    it('should send a 422 status because the body field password is incorrect', done => {
      request
        .post('/emails')
        .send({
          email: 'Fulanito@mail.com',
          password: ''
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid password field');
          done();
        });
    });
  });

  describe('- Testing validFunctionBody middleware', () => {
    before(() => {
      server.post(
        '/functions',
        validFunctionBody,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: '12:00',
          start_date: Date(),
          finish_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          price: 60,
          active: true
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field movie_schedule is incorrect', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: '1200',
          start_date: Date(),
          finish_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          price: 60,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid movie_schedule field');
          done();
        });
    });

    it('should send a 422 status because the body field movie_schedule contain letters', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: 'b2:a0',
          start_date: Date(),
          finish_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          price: 60,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid movie_schedule field');
          done();
        });
    });

    it('should send a 422 status because the body field movie_schedule is empty', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: '',
          start_date: Date(),
          finish_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          price: 60,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid movie_schedule field');
          done();
        });
    });

    it('should send a 422 status because the body field start_date is empty', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: '12:00',
          start_date: null,
          finish_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          price: 60,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid start_date field');
          done();
        });
    });

    it('should send a 422 status because the body field finish_date is empty', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: '12:00',
          start_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          finish_date: null,
          price: 60,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid finish_date field');
          done();
        });
    });

    it('should send a 200 status because the body field price is valid', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: '12:00',
          start_date: Date(),
          finish_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          price: 12.5,
          active: true
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field price is invalid', done => {
      request
        .post('/functions')
        .send({
          movie_schedule: '12:00',
          start_date: Date(),
          finish_date: new Date(2018, 10, 30, 0, 0, 0).toString(),
          price: 'a60jhghg',
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid price field');
          done();
        });
    });
  });

  describe('- Testing validRoomBody middleware', () => {
    before(() => {
      server.post(
        '/rooms',
        validRoomBody,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/rooms')
        .send({
          type: 'standar',
          capacity: 100
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field type is empty', done => {
      request
        .post('/rooms')
        .send({
          type: '',
          capacity: 100
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid type field');
          done();
        });
    });

    it('should send a 422 status because the body field type is empty', done => {
      request
        .post('/rooms')
        .send({
          type: '',
          capacity: 100
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid type field');
          done();
        });
    });

    it('should send a 422 status because the body field capacity is less than 10', done => {
      request
        .post('/rooms')
        .send({
          type: 'standar',
          capacity: 9
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('should not be less than 10');
          done();
        });
    });

    it('should send a 422 status because the body field capacity is empty', done => {
      request
        .post('/rooms')
        .send({
          type: 'standar',
          capacity: null
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid capacity field');
          done();
        });
    });
  });

  describe('- Testing validSeatBody middleware', () => {
    before(() => {
      server.post(
        '/seats',
        validSeatBody,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/seats')
        .send({
          seats: 1
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field seats is empty', done => {
      request
        .post('/seats')
        .send({
          seats: null
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid seats field');
          done();
        });
    });

    it('should send a 422 status because the body field seats is less than 1', done => {
      request
        .post('/seats')
        .send({
          seats: -1
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('should not be less than 1');
          done();
        });
    });
  });
  describe('- Testing validTicketDetailBody middleware', () => {
    before(() => {
      server.post(
        '/ticketDetails',
        validTicketDetailBody,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/ticketDetails')
        .send({
          card_number: '123456789012345',
          email: 'fulanito@correo.com'
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field card_number length is less than 16', done => {
      request
        .post('/ticketDetails')
        .send({
          card_number: '12345678901234',
          email: 'fulanito@correo.com'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid card_number field');
          done();
        });
    });

    it('should send a 422 status because the body field card_number contain letters', done => {
      request
        .post('/ticketDetails')
        .send({
          card_number: 'a12345d789012o4',
          email: 'fulanito@correo.com'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid card_number field');
          done();
        });
    });

    it('should send a 422 status because the body field card_number is empty', done => {
      request
        .post('/ticketDetails')
        .send({
          card_number: '',
          email: 'fulanito@correo.com'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid card_number field');
          done();
        });
    });
  });

  describe('- Testing userRoleAuth middleware', () => {
    before(() => {
      server.get(
        '/roles',
        userRoleAuth({
          [USERTYPES.ADMIN]: {
            GET: [/\/roles\/?/]
          },
          [USERTYPES.SELLER]: {
            GET: [/\/roles\/?/]
          },
          [USERTYPES.CLIENT]: { GET: [/\/(^roles)\/?/] },
          [USERTYPES.DEFAULT]: {}
        }),
        (req, res, netx) => res.send(Date()),
        errorHandler
      );
    });

    it('should send a 200 status because the user_type is admin', done => {
      request
        .get('/roles')
        .set('user_type', USERTYPES.ADMIN)
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 200 status because the user_type is seller', done => {
      request
        .get('/roles')
        .set('user_type', USERTYPES.SELLER)
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 403 status because the user_type is client', done => {
      request
        .get('/roles')
        .set('user_type', USERTYPES.CLIENT)
        .expect(403)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('user do not have access');
          done();
        });
    });

    it('should send a 403 status because the user_type is default', done => {
      request
        .get('/roles')
        .set('user_type', USERTYPES.DEFAULT)
        .expect(403)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain(
            'user do not have access to that method request'
          );
          done();
        });
    });

    it('should send a 403 status because the user_type is not set', done => {
      request
        .get('/roles')
        .expect(403)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain(
            'user do not have access to that method request'
          );
          done();
        });
    });

    it('should send a 403 status because the user_type is not in the roles', done => {
      request
        .get('/roles')
        .expect(403)
        .set('user_type', 100)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid user role');
          done();
        });
    });
  });

  describe('- Testing enableCors middleware', () => {
    before(() => {
      server.get(
        '/cors',
        enableCors,
        (req, res, next) => res.send(Date()),
        errorHandler
      );
    });

    it('should find the header Access-Control-Allow-Origin', done => {
      request
        .get('/cors')
        .expect(200)
        .expect('Access-Control-Allow-Origin', '*')
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should find the header Access-Control-Allow-Headers', done => {
      request
        .get('/cors')
        .expect(200)
        .expect(
          'Access-Control-Allow-Headers',
          'Origin, X-Requested-With, Content-Type, Accept'
        )
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });
  });

  after(() => {
    delete server;
    delete request;
  });
});
