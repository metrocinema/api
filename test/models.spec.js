const { it, describe, before, after } = require('mocha');
const { expect, should, assert } = require('chai');
const supertest = require('supertest');
const { Users, connector } = require('../src/models');

describe('#Testing functionality of the ORM models', () => {
  describe('- Testing Users model', () => {
    let user;
    it('should create the users table', done => {
      Users.createTable()
        .then(res => {
          expect(res).to.be.a('string');
          expect(res).to.be.equals('Success');
          done();
        })
        .catch(err => done(err));
    });

    it('should insert a user into the table', done => {
      Users.insert({
        first_name: 'fulanito',
        last_name: 'fulano',
        email: 'fulanito@correo.com',
        password: '12345',
        user_type: 1,
        active: 1
      })
        .then(res => {
          expect(res).to.have.property('insertId');
          expect(res.insertId).to.be.a('number');
          done();
        })
        .catch(done);
    });

    it('should get all users', done => {
      Users.find()
        .then(res => {
          expect(res).to.be.an('array');
          expect(res).to.not.have.lengthOf(0);
          [user] = res;
          done();
        })
        .catch(done);
    });

    it('should get one user', done => {
      Users.find(user.id_user)
        .then(res => {
          expect(res).to.have.property('first_name');
          expect(res.first_name).to.be.equals('fulanito');
          done();
        })
        .catch(done);
    });

    it('should delete a user', done => {
      Users.deleteById(user.id_user)
        .then(deleted => {
          expect(deleted).to.have.property('changedRows');
          expect(deleted.changedRows).to.be.equals(1);
          done();
        })
        .catch(done);
    });

    it('should update a user active status', done => {
      Users.updateById(user.id_user, {
        // ...user,
        active: 1
      })
        .then(updated => {
          expect(updated).to.have.property('changedRows');
          expect(updated.changedRows).to.be.equals(1);
          done();
        })
        .catch(done);
    });
  });

  after(() => connector.end(console.error));
});
